# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( xAODTrigMinBiasCnv )

atlas_add_library( xAODTrigMinBiasCnvLib
                   xAODTrigMinBiasCnv/*.h
                   INTERFACE
                   PUBLIC_HEADERS xAODTrigMinBiasCnv
                   LINK_LIBRARIES GaudiKernel xAODTrigMinBias )

# Component(s) in the package:
atlas_add_component( xAODTrigMinBiasCnv
                     src/*.cxx
                     src/components/*.cxx
                     LINK_LIBRARIES AthenaBaseComps AthenaKernel TrigCaloEvent TrigInDetEvent xAODTrigMinBiasCnvLib )

